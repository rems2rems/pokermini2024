import { describe, expect, it } from "vitest";
import { categorizeHand, compare } from "../src/hand";

describe("hand comparison", () => {

    it("must compare a high-card vs itself",()=>{
        const highCard = categorizeHand([{rank:"A",suit:"♠"},{rank:"Q",suit:"♠"},{rank:"T",suit:"♥"}])
        
        const winner = compare(highCard,highCard)
        expect(winner).toBe(null)
    })
    it("must compare a high-card hand vs a pair hand",()=>{
        const highCard = categorizeHand([{rank:"A",suit:"♠"},{rank:"Q",suit:"♠"},{rank:"T",suit:"♥"}])
        const pair = categorizeHand([{rank:"A",suit:"♠"},{rank:"Q",suit:"♠"},{rank:"A",suit:"♥"}])

        const winner = compare(highCard,pair)
        expect(winner.type).toBe("Pair")
    })
})