import { describe, expect, it } from "vitest";
import { categorizeHand } from "../src/hand";

describe("Hand", () => {
    it("should categorize a high-card hand", () => {
        const cards = [{rank:"A",suit:"♠"},{rank:"Q",suit:"♠"},{rank:"T",suit:"♥"}]
        const hand = categorizeHand(cards)

        expect(hand.type).toBe("High-Card")
        expect(hand.cards).toBe(cards)
    })
    it("should categorize a pair hand", () => {
        const cards = [{rank:"A",suit:"♠"},{rank:"Q",suit:"♠"},{rank:"A",suit:"♥"}]
        const hand = categorizeHand(cards)

        expect(hand.type).toBe("Pair")
        expect(hand.rank).toBe("A")
        expect(hand.kicker).toBe("Q")
        expect(hand.cards).toBe(cards)

    })
})