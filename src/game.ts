export function createNewGame(){
    return {
        balances: {
          human: 100,
          bot: 100,
        },
        hand: {
          stage: "ante",
          currentPlayer: "human",
          pot: 0,
          bets: {
            human: 0,
            bot: 0,
          },
        },
      };
}