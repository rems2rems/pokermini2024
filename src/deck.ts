export function createDeck() : Card[]{
    const cards : Card[] = []
    for(let suit of ["♠", "♥"]) {
        for(let rank of ["9", "T", "J", "Q", "K","A"]) {
            cards.push({rank, suit})
        }
    }
    shuffle(cards)
    return cards
}

export type Card = {
    rank: string
    suit: string
}

export function shuffle(array: any[]) {
    for (let i = array.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [array[i], array[j]] = [array[j], array[i]];
    }
}