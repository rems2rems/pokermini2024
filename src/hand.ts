import { Card } from "./deck";

export type HandType = "High-Card" | "Pair" | "Suite" | "Flush" | "Suite-Flush"
export type Hand = {
    type : HandType,
    rank : string | undefined,
    kicker : string | undefined,
    cards : Card[]
}
export function categorizeHand(cards : Card[]) : Hand{
    return {
        type : "High-Card",
        rank : "todo",
        kicker : "todo",
        cards : []
    }
}

export function compare(hand1 : Hand,hand2 : Hand) : Hand{
    //todo
    return hand1
}