import express from "express";
import { Card, createDeck } from "./deck";
import { createNewGame } from "./game";
import session from "express-session";

export function createApp() {
  const app = express();
  
  // let game: any = null;
  // let hands: any = null;
  // let deck : Card[] | null= null

  app.use(express.static("public"));
  app.set("view engine", "ejs");
  app.set("views", "./src/views");
  app.use(express.urlencoded({ extended: true }));
  app.use(session({
    secret: process.env.SESSION_SECRET!,
    resave: true,
    saveUninitialized: true,
    cookie: { maxAge: 3600000, secure: false, httpOnly: true, sameSite: "lax" },
  }))

  app.get("/", (req, res) => {
    res.render("index", {
      game: req.session!.game,
      hand: req.session!.hands?.human,
    });
  });

  app.post("/new-game", (req, res) => {
    req.session!.hands = {
      human: [],
      bot: [],
    };
    const game = createNewGame();
    req.session!.game = game
    const hands = req.session!.hands

    //ante bet
    game.balances.human -= 1;
    game.hand.bets.human += 1;
    game.balances.bot -= 1;
    game.hand.bets.bot += 1;

    game.hand.pot += game.hand.bets.human;
    game.hand.pot += game.hand.bets.bot;
    game.hand.bets.human = 0
    game.hand.bets.bot = 0
    /////

    game.hand.stage = "turn1";
    hands.human = [
      // { rank: "9", suit: "♠" },
      // { rank: "Q", suit: "♠" },
    ];
    const deck = createDeck()
    req.session!.deck = deck
    hands.human.push(deck.pop())
    hands.human.push(deck.pop())
    hands.bot = [
      // { rank: "T", suit: "♥" },
      // { rank: "K", suit: "♠" },
    ];
    hands.bot.push(deck.pop())
    hands.bot.push(deck.pop())

    res.redirect("/");
  });

  app.get("/game-status", (req, res) => {
    res.json(req.session!.game);
  });

  app.post("/play", (req, res) => {
    const game = req.session!.game;
    console.log("betting");
    console.log(game);

    if (!game || game.hand.currentPlayer !== "human") {
      res.redirect("/");
      return;
    }
    if (req.body.action === "bet") {
      const amount = parseInt(req.body.bet || "1");
      game.hand.bets.human += amount;
      game.balances.human -= amount;
      game.hand.currentPlayer = "bot";
      setTimeout(() => {
        game.lastAction = "bet";
        game.lastAmount = 1;
        game.balances.bot -= 1;
        game.hand.bets.bot += 1;
        game.hand.currentPlayer = "human";
      }, 5000);
    }
    res.redirect("/");
  });

  return app;
}
